'''
Created on Sep 15, 2013

@author: antonio
'''
from django.contrib import admin
from polls.models import Poll, Choice

admin.site.register(Poll)
admin.site.register(Choice)